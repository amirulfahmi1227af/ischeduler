@extends('layouts.app')

@section('content')
@include('layouts.headers.cards')
<link rel="stylesheet" type="text/css" href="{{ asset('argon') }}/css/table.css">
<div class="container">
                <div class="timetable-img text-center">
                    <img src="img/content/timetable.png" alt="">
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered text-center">
                        <thead>
                            <tr class="bg-light-gray">
                                <th class="text-uppercase">Time
                                </th>
                                <th class="text-uppercase">Monday</th>
                                <th class="text-uppercase">Tuesday</th>
                                <th class="text-uppercase">Wednesday</th>
                                <th class="text-uppercase">Thursday</th>
                                <th class="text-uppercase">Friday</th>
                                <th class="text-uppercase">Saturday</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="align-middle">09:00am</td>
                                <td>
                                    <span class="bg-sky padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16 xs-font-size13">Dance</span>
                                    <div class="margin-10px-top font-size14">9:00-10:00</div>
                                    <div class="font-size13 text-light-gray">Ivana Wong</div>
                                </td>
                                <td>
                                    <span class="bg-green padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Yoga</span>
                                    <div class="margin-10px-top font-size14">9:00-10:00</div>
                                    <div class="font-size13 text-light-gray">Marta Healy</div>
                                </td>

                                <td>
                                    <span class="bg-yellow padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Music</span>
                                    <div class="margin-10px-top font-size14">9:00-10:00</div>
                                    <div class="font-size13 text-light-gray">Ivana Wong</div>
                                </td>
                                <td>
                                    <span class="bg-sky padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Dance</span>
                                    <div class="margin-10px-top font-size14">9:00-10:00</div>
                                    <div class="font-size13 text-light-gray">Ivana Wong</div>
                                </td>
                                <td>
                                    <span class="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Art</span>
                                    <div class="margin-10px-top font-size14">9:00-10:00</div>
                                    <div class="font-size13 text-light-gray">Kate Alley</div>
                                </td>
                                <td>
                                    <span class="bg-pink padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">English</span>
                                    <div class="margin-10px-top font-size14">9:00-10:00</div>
                                    <div class="font-size13 text-light-gray">James Smith</div>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle">10:00am</td>
                                <td>
                                    <span class="bg-yellow padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Music</span>
                                    <div class="margin-10px-top font-size14">10:00-11:00</div>
                                    <div class="font-size13 text-light-gray">Ivana Wong</div>
                                </td>
                                <td class="bg-light-gray">

                                </td>
                                <td>
                                    <span class="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Art</span>
                                    <div class="margin-10px-top font-size14">10:00-11:00</div>
                                    <div class="font-size13 text-light-gray">Kate Alley</div>
                                </td>
                                <td>
                                    <span class="bg-green padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Yoga</span>
                                    <div class="margin-10px-top font-size14">10:00-11:00</div>
                                    <div class="font-size13 text-light-gray">Marta Healy</div>
                                </td>
                                <td>
                                    <span class="bg-pink padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">English</span>
                                    <div class="margin-10px-top font-size14">10:00-11:00</div>
                                    <div class="font-size13 text-light-gray">James Smith</div>
                                </td>
                                <td class="bg-light-gray">

                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle">11:00am</td>
                                <td>
                                    <span class="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Break</span>
                                    <div class="margin-10px-top font-size14">11:00-12:00</div>
                                </td>
                                <td>
                                    <span class="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Break</span>
                                    <div class="margin-10px-top font-size14">11:00-12:00</div>
                                </td>
                                <td>
                                    <span class="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Break</span>
                                    <div class="margin-10px-top font-size14">11:00-12:00</div>
                                </td>
                                <td>
                                    <span class="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Break</span>
                                    <div class="margin-10px-top font-size14">11:00-12:00</div>
                                </td>
                                <td>
                                    <span class="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Break</span>
                                    <div class="margin-10px-top font-size14">11:00-12:00</div>
                                </td>
                                <td>
                                    <span class="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Break</span>
                                    <div class="margin-10px-top font-size14">11:00-12:00</div>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle">12:00pm</td>
                                <td class="bg-light-gray">

                                </td>
                                <td>
                                    <span class="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Art</span>
                                    <div class="margin-10px-top font-size14">12:00-1:00</div>
                                    <div class="font-size13 text-light-gray">Kate Alley</div>
                                </td>
                                <td>
                                    <span class="bg-sky padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Dance</span>
                                    <div class="margin-10px-top font-size14">12:00-1:00</div>
                                    <div class="font-size13 text-light-gray">Ivana Wong</div>
                                </td>
                                <td>
                                    <span class="bg-yellow padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Music</span>
                                    <div class="margin-10px-top font-size14">12:00-1:00</div>
                                    <div class="font-size13 text-light-gray">Ivana Wong</div>
                                </td>
                                <td class="bg-light-gray">

                                </td>
                                <td>
                                    <span class="bg-green padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Yoga</span>
                                    <div class="margin-10px-top font-size14">12:00-1:00</div>
                                    <div class="font-size13 text-light-gray">Marta Healy</div>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle">01:00pm</td>
                                <td>
                                    <span class="bg-pink padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">English</span>
                                    <div class="margin-10px-top font-size14">1:00-2:00</div>
                                    <div class="font-size13 text-light-gray">James Smith</div>
                                </td>
                                <td>
                                    <span class="bg-yellow padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Music</span>
                                    <div class="margin-10px-top font-size14">1:00-2:00</div>
                                    <div class="font-size13 text-light-gray">Ivana Wong</div>
                                </td>
                                <td class="bg-light-gray">

                                </td>
                                <td>
                                    <span class="bg-pink padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">English</span>
                                    <div class="margin-10px-top font-size14">1:00-2:00</div>
                                    <div class="font-size13 text-light-gray">James Smith</div>
                                </td>
                                <td>
                                    <span class="bg-green padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Yoga</span>
                                    <div class="margin-10px-top font-size14">1:00-2:00</div>
                                    <div class="font-size13 text-light-gray">Marta Healy</div>
                                </td>
                                <td>
                                    <span class="bg-yellow padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom text-white font-size16  xs-font-size13">Music</span>
                                    <div class="margin-10px-top font-size14">1:00-2:00</div>
                                    <div class="font-size13 text-light-gray">Ivana Wong</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
<!--Reminder-->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('argon') }}/css/reminder.css">
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="widget widget-reminder">
                <div class="widget-reminder-header">TODAY, NOV 4</div>
                <div class="widget-reminder-container">
                    <div class="widget-reminder-time">
                        09:00<br />
                        12:00
                    </div>
                    <div class="widget-reminder-divider bg-success"></div>
                    <div class="widget-reminder-content">
                        <h4 class="widget-title">Meeting with HR</h4>
                        <div class="widget-desc"><i class="fa fa-map-pin"></i> Conference Room</div>
                    </div>
                </div>
                <div class="widget-reminder-container">
                    <div class="widget-reminder-time">
                        20:00<br />
                        23:00
                    </div>
                    <div class="widget-reminder-divider bg-primary"></div>
                    <div class="widget-reminder-content">
                        <h4 class="widget-title">Dinner with Richard</h4>
                        <div class="widget-desc"><i class="fa fa-map-pin"></i> Tom's Too Restaurant</div>
                        <div class="m-t-15">
                            <a href="#" class="pull-right">Contact</a>
                            <img src="https://bootdey.com/img/Content/avatar/avatar1.png" width="16" class="img-circle pull-left m-r-5" alt="" /> Richard Leong
                        </div>
                    </div>
                </div>
                <div class="widget-reminder-header">TOMORROW, NOV 5</div>
                <div class="widget-reminder-container">
                    <div class="widget-reminder-time">All day</div>
                    <div class="widget-reminder-divider bg-purple"></div>
                    <div class="widget-reminder-content">
                        <h4 class="widget-title"><i class="fa fa-gift text-purple"></i> Terry Birthday</h4>
                    </div>
                </div>
                <div class="widget-reminder-container">
                    <div class="widget-reminder-time">
                        00:00<br />
                        00:30
                    </div>
                    <div class="widget-reminder-divider bg-danger"></div>
                    <div class="widget-reminder-content">
                        <h4 class="widget-title">Server Maintenance</h4>
                        <div class="widget-desc"><i class="ti-pin"></i> Data Centre</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="widget widget-reminder">
                <div class="widget-reminder-header">TODAY, NOV 5</div>
                <div class="widget-reminder-container">
                    <div class="widget-reminder-time">
                        09:00<br />
                        12:00
                    </div>
                    <div class="widget-reminder-divider bg-success"></div>
                    <div class="widget-reminder-content">
                        <h4 class="widget-title">Meeting with HR</h4>
                        <div class="widget-desc"><i class="fa fa-map-pin"></i> Conference Room</div>
                    </div>
                </div>
                <div class="widget-reminder-container">
                    <div class="widget-reminder-time">
                        20:00<br />
                        23:00
                    </div>
                    <div class="widget-reminder-divider bg-primary"></div>
                    <div class="widget-reminder-content">
                        <h4 class="widget-title">Dinner with Richard</h4>
                        <div class="widget-desc"><i class="fa fa-map-pin"></i> Tom's Too Restaurant</div>
                        <div class="m-t-15">
                            <a href="#" class="pull-right">Contact</a>
                            <img src="https://bootdey.com/img/Content/avatar/avatar6.png" width="16" class="img-circle pull-left m-r-5" alt="" /> John Doe
                        </div>
                    </div>
                </div>
                <div class="widget-reminder-header">TOMORROW, NOV 5</div>
                <div class="widget-reminder-container">
                    <div class="widget-reminder-time">All day</div>
                    <div class="widget-reminder-divider bg-purple"></div>
                    <div class="widget-reminder-content">
                        <h4 class="widget-title"><i class="fa fa-gift text-purple"></i> Terry Birthday</h4>
                    </div>
                </div>
                <div class="widget-reminder-container">
                    <div class="widget-reminder-time">
                        00:00<br />
                        00:30
                    </div>
                    <div class="widget-reminder-divider bg-danger"></div>
                    <div class="widget-reminder-content">
                        <h4 class="widget-title">Server Maintenance</h4>
                        <div class="widget-desc"><i class="ti-pin"></i> Data Centre</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  @include('layouts.footers.auth')
@endsection
