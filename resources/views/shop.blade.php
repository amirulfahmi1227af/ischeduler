@extends('layouts.app')
@section('content')
@include('layouts.headers.cards')

<link rel="stylesheet" type="text/css" href="{{ asset('argon') }}/css/shop.css">
<div class="container bootstrap snipets">
   <h1 class="text-center text-muted">SHOP</h1>
   <div class="row flow-offset-1">
     <div class="col-xs-6 col-md-4">
       <div class="product tumbnail thumbnail-3"><a href="#"><img src="\assets\img\timetable1.jpg" alt="timetable1" width="350" height="280"></a>
         <div class="caption">
           <h6><a href="#">Timetable 1</a></h6><span class="price">
             <del>1000 Coin</del></span><span class="price sale">500 Coin</span>
         </div>
       </div>
     </div>
     <div class="col-xs-6 col-md-4">
       <div class="product tumbnail thumbnail-3"><a href="#"><img src="\assets\img\timetable2.jpg" alt="timetable2" width="350" height="280"></a>
         <div class="caption">
           <h6><a href="#">Timetable 2</a></h6><span class="price">
             <del>1200 Coin</del></span><span class="price sale">600 Coin</span>
         </div>
       </div>
     </div>
     <div class="col-xs-6 col-md-4">
       <div class="product tumbnail thumbnail-3"><a href="#"><img src="\assets\img\timetable3.jpg" alt="timetable3" width="350" height="280"></a>
         <div class="caption">
           <h6><a href="#">Timetable 3</a></h6><span class="price">1200 Coin</span>
         </div>
       </div>
     </div>
     <div class="col-xs-6 col-md-4">
       <div class="product tumbnail thumbnail-3"><a href="#"><img src="\assets\img\timetable4.jpg" alt="timetable4" width="350" height="280"></a>
         <div class="caption">
           <h6><a href="#">Timetable 4</a></h6><span class="price">
             <del>1400 Coin</del></span><span class="price sale">800 Coin</span>
         </div>
       </div>
     </div>
     <div class="col-xs-6 col-md-4">
       <div class="product tumbnail thumbnail-3"><a href="#"><img src="\assets\img\timetable5.jpg" alt="timetable5" width="350" height="280"></a>
         <div class="caption">
           <h6><a href="#">Timetable 5</a></h6><span class="price">
             <del>1200 Coin</del></span><span class="price sale">700 Coin</span>
         </div>
       </div>
     </div>
     <div class="col-xs-6 col-md-4">
       <div class="product tumbnail thumbnail-3"><a href="#"><img src="\assets\img\timetable6.jpg" alt="timetable6" width="350" height="280"></a>
         <div class="caption">
           <h6><a href="#">Timetable 6</a></h6><span class="price">
             <del>1200 Coin</del></span><span class="price sale">550 Coin</span>
         </div>
       </div>
     </div>
     <div class="col-xs-6 col-md-4">
       <div class="product tumbnail thumbnail-3"><a href="#"><img src="\assets\img\timetable7.jpg" alt="timetable6" width="350" height="280"></a>
         <div class="caption">
           <h6><a href="#">Timetable 7</a></h6><span class="price">1400 Coin</span>
         </div>
       </div>
     </div>
     <div class="col-xs-6 col-md-4">
       <div class="product tumbnail thumbnail-3"><a href="#"><img src="\assets\img\timetable8.jpg" alt="timetable6" width="350" height="280"></a>
         <div class="caption">
           <h6><a href="#">Timetable 8</a></h6><span class="price">
             <del>1400 Coin</del></span><span class="price sale">1200 Coin</span>
         </div>
       </div>
     </div>
     <div class="col-xs-6 col-md-4">
       <div class="product tumbnail thumbnail-3"><a href="#"><img src="\assets\img\timetable9.jpg" alt="timetable6" width="350" height="280"></a>
         <div class="caption">
           <h6><a href="#">Timetable 9</a></h6><span class="price">
             <del>1500 Coin</del></span><span class="price sale">600 Coin</span>
         </div>
       </div>
     </div>
   </div>
 </div>
@include('layouts.footers.auth')
@endsection
