<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProductController;
use App\Http\Controllers\FullCalenderController;
use App\Http\Controllers\TaskController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::get('upgrade', function () {return view('pages.upgrade');})->name('upgrade');
	Route::get('map', function () {return view('pages.maps');})->name('map');
	Route::get('icons', function () {return view('pages.icons');})->name('icons');
	Route::get('table-list', function () {return view('pages.tables');})->name('table-list');

  //route for iScheduler
  Route::get('table', function () {return view('table');})->name('table');
  Route::get('task', function () {return view('task');})->name('task');
  Route::get('calendar', function () {return view('calendar');})->name('calendar');
  Route::get('shop', function () {return view('shop');})->name('shop');
  Route::get('settings', function () {return view('settings');})->name('settings');
  //route for iScheduler(calendar)
  Route::get('fullcalender', [App\Http\Controllers\FullCalenderController::class, 'index'])->name('fullcalender');
  Route::post('fullcalenderAjax', [App\Http\Controllers\FullCalenderController::class, 'ajax']);

  //route for taskController
  Route::resource('tasks', TaskController::class);
  Route::resource('products', ProductController::class);

  Route::get('timetable', function () {return view('timetable');})->name('timetable');

  Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});
